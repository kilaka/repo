package info.fastpace.repo;

/*
Modules:
- Harvester - Harvest open source libraries. TODO: Handle versioning.
- Downloader - Download classes from the internet and add them to the Repo (should include dependencies).
- Indexer/Search - indexes the repo for fast search.
- Importer - Import files
  - Copy class and dependencies.
  - Create branch for class and dependencies.
  - Mark dependency in some config file.
- Scanner - scan files for changed imports and delete/import accordingly.
*/

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


public class ClassImporter {
	private static final HashSet<String> IGNORED_PACKAGE_PREFIXES = new HashSet<>(Arrays.asList(new String[]{
			"java"
//			, "sun"
	}));

//	private static final HashSet<String> IMPORTED_CLASS_NAMES = new HashSet<>();

	public static void main(String[] args) throws Exception {
		File depSourceFolder = new File("dep-source");
//		FileUtils.deleteDirectory(depSourceFolder);
		File depFolder = new File("dep");
//		FileUtils.deleteDirectory(depFolder);
		importClass("org.apache.commons.io.FileUtils");
//		importClass("info.fastpace.Temp");
	}

	public static void importClass(String className) throws Exception {
		if (className.endsWith(".*")) {
			throw new Exception("Found import class that cannot be handled: " + className);
		}
		String packagePrefix = className.substring(0, className.indexOf('.'));
		if (IGNORED_PACKAGE_PREFIXES.contains(packagePrefix)) {
			return;
		}
		int lastIndexOfDot = className.lastIndexOf('.');
		String shortName = className.substring(lastIndexOfDot + 1);
		String packageName = className.substring(0, lastIndexOfDot);
		File sourceFolder = new File("repo/" + className);
		if (!sourceFolder.exists()) {
			throw new Exception("Failed to find class to import: " + className);
		}
		File depSourceFolder = new File("dep-source/" + className);
		if (depSourceFolder.exists()) {
			return;
		}
		System.out.println("Importing class: " + className + "...");
		FileUtils.copyDirectory(sourceFolder, depSourceFolder);
		File depSource = new File(depSourceFolder, shortName + ".java");
		File dest = new File("dep/" + packageName.replace('.', '/'), depSource.getName());
		if (dest.exists()) {
			return;
		}
		dest.getParentFile().mkdirs();
		Files.createSymbolicLink(Paths.get(dest.toURI()), Paths.get(depSource.toURI()));
		try {
			importImports(dest);
		} catch (Exception e) {
			throw new Exception("Failed to import class: " + className, e);
		}
		System.out.println("Finished importing class: " + className);
	}

	private static void importImports(final File javaFile) throws Exception {
		final CompilationUnit compilationUnit = JavaParser.parse(javaFile);

		final HashSet<String> importedClasses = new HashSet<>();
		importedClasses.add(FilenameUtils.getBaseName(javaFile.getName()));
		// Import imports
		List<ImportDeclaration> imports = compilationUnit.getImports();
		if (imports != null) {
			for (ImportDeclaration importDeclaration : imports) {
				importClass(importDeclaration.getName().toString());
				importedClasses.add(importDeclaration.getName().getName());
			}
		}
		// Import same package classes
		final HashSet<String> implicitImports = new HashSet<>();
		final HashSet<String> explicitImports = new HashSet<>();
		new VoidVisitorAdapter<Object>() {
			@Override
			public void visit(ClassOrInterfaceType classOrInterfaceType, Object arg) {
				String shortClassName = classOrInterfaceType.getName();
				if (!shortClassName.matches("^[A-Z].*")) {
					return;
				}
				if (classOrInterfaceType.getScope() != null) {
					explicitImports.add(classOrInterfaceType.getScope().toString() + '.' + shortClassName);
					return;
				}
				if (shortClassName.length() == 1) { // Generic class. No need to import. TODO: Think of another way to check if generics.
					return;
				}
				if (importedClasses.contains(shortClassName)) { // Already imported
					return;
				}
				try {
					String tryJavaLang = "java.lang." + shortClassName;
					Class.forName(tryJavaLang);
					return; // Found, so return.
				} catch (ClassNotFoundException e) {
				}
				implicitImports.add(shortClassName);
			}
			@Override
			public void visit(NameExpr nameExpr, Object arg) {
				String className = nameExpr.getName();
				if (!className.matches("^[A-Z].*")) {
					return;
				}
				if (classExists(compilationUnit.getPackage().getName() + "." + className)) {
					implicitImports.add(className);
				}
			}
		}.visit(compilationUnit, null);

		for (String implicitImport : implicitImports) {
			importClass(compilationUnit.getPackage().getName() + "." + implicitImport);
			importedClasses.add(implicitImport);
		}
		for (String explicitImport : explicitImports) {
			importClass(explicitImport);
		}
	}

	private static boolean classExists(String className) {
		File sourceFolder = new File("repo/" + className);
		return sourceFolder.exists();
	}
//	private static void importImports(String className) throws Exception {
//		URLClassLoader urlClassLoader = URLClassLoader.newInstance(new URL[]{new File("dep").getAbsoluteFile().toURL()});
//		Class < ? > clas = urlClassLoader.loadClass(className);
////		clas.getDe
//		System.out.println(clas);
//	}
}
