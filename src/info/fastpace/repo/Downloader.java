package info.fastpace.repo;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by elzin on 5/28/2015.
 */
public class Downloader {
	public static void main(String[] args) throws IOException {
		addSourceFolderToRepo(new File("libs\\commons-io-2.4-src\\src\\main\\java"));
//		addJavaFileToRepo(new File("libs\\commons-io-2.4-src\\src\\main\\java\\org\\apache\\commons\\io\\IOUtils.java"));
	}

	private static void addSourceFolderToRepo(File sourceFolder) throws IOException {
		Collection<File> files = FileUtils.listFiles(sourceFolder, new String[]{"java"}, true);
		for (File file : files) {
			addJavaFileToRepo(file);
		}
	}

	private static void addJavaFileToRepo(File javaFile) throws IOException {
		for (LineIterator lineIterator = FileUtils.lineIterator(javaFile) ; lineIterator.hasNext() ;) {
			String line = lineIterator.next();
			line = line.trim();
			if (!line.startsWith("package ")) {
				continue;
			}
			int semiColumnIndex = line.indexOf(';');
			if (semiColumnIndex < 0) {
				System.err.println("Found package that cannot be handled: " + line);
				return;
			}
			String packageName = line.substring("package ".length(), semiColumnIndex);
			packageName = packageName.trim();
			String className = FilenameUtils.removeExtension(javaFile.getName());
			File repoTargetFolder = new File("repo/"+packageName+'.'+className);
			File repoTarget = new File(repoTargetFolder, javaFile.getName());
			FileUtils.copyFile(javaFile, repoTarget);
			return;
		}
	}
}
